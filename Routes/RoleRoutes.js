const express = require("express")
const RoleController = require("./../Controllers/roleController")
const router = express.Router();

router.route("/").get(RoleController.getAllRoles).post(RoleController.createRole)
router.route("/:id").get(RoleController.getRoleById).put(RoleController.updateRole).delete(RoleController.deleteRole);

module.exports = router
