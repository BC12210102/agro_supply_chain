const express = require("express")
const router = express.Router()
const authController = require("./../Controllers/authController")
const viewsController = require("./../Controllers/viewController")


// users
router.get("/login", viewsController.getLoginForm)
router.get("/", viewsController.getSignupForm)

router.get("/home", viewsController.getHome)
router.get("/userHome", viewsController.getUserHome)

// ADMIN
router.get("/admin", viewsController.getAdminLogin)
router.get("/admin/signupSecret", viewsController.getAdminSignup)
router.get("/admin/home", viewsController.getAdminPage)

router.get("/admin/users", viewsController.getUsers)
router.get("/admin/settings", viewsController.getAdmSetting)



module.exports = router