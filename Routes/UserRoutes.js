const express = require("express");
const UserController = require("./../Controllers/UserController");
const authController = require("./../Controllers/authController");
const router = express.Router();
router.post("/signup", authController.signup);
router.post("/login", authController.login);

router.get("/logout", authController.logout);
router.patch("/updateMyPassword/:id", authController.updatePassword);

router
    .route("/")
    .get(UserController.getAllUsers)
    .post(UserController.createUser);

router
  .route("/")
  .get(UserController.getAllUsers)
  .post(UserController.createUser);
router
    .route("/deactivate/:id")
    .put(UserController.deactivate);
router
  .route("/:id")
  .get(UserController.getUserById)
  .put(UserController.updateUser)
  .delete(UserController.deleteUser)
  .put(UserController.uploadUserPhoto, UserController.updateUser);
module.exports = router;
