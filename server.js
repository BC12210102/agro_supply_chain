const dotenv = require("dotenv")
dotenv.config({ path: "./config.env" })
const mongoose = require("mongoose")
const app = require("./app")

const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.DATABASE_PASSWORD,
)

mongoose.connect(DB).then((con) => {
    console.log(con.connections)
    console.log("DB connection successful")
}).catch(error => console.log(error));

// Specify the server port
const port = process.env.PORT || 4001
// Start listening for incoming request on the web
app.listen(port, () => {
    console.log(`App running on port : ${port} ..`)
})