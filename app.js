const express = require("express")
const path = require("path")
const app = express()
const UserRouter = require("./Routes/UserRoutes")
const viewRouter = require("./Routes/viewRoutes")

app.use(express.json())
app.use(express.static(__dirname + "views"))
app.use("/", viewRouter)
app.use("/api/v1/users", UserRouter)

app.use(express.static(path.join(__dirname, "views")))

module.exports = app