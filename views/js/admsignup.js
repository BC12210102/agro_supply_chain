import { showAlert } from "./alert.js"

const getusers = await axios({
    method: "GET",
    url: "https://agro-chain.onrender.com/api/v1/users"
})
const allusers = getusers.data.data

const admins = []
allusers.forEach(user => {
    if (user.role_id === 1) {
        admins.push(user)
    }
})
console.log(admins)

document.querySelector(".form").addEventListener("submit", (e) => {
    e.preventDefault()
    const name = document.getElementById("fname").value
    const username = document.getElementById("ename").value
    const email = document.getElementById("email").value
    const password = document.getElementById("password").value
    const passwordConfirm = document.getElementById("passwordC").value

    let pattern = /^[^ ]+\.[gcit]{4}@[rub]{3}\.[edu]{3}\.[bt]{2}$/;
    const email_valid = (email.match(pattern))
    if (!email_valid) {
        showAlert("error", "Please enter your valid email")
    } else {
        if (password.length <= 7) {
            showAlert("error", "Password must be longer than 7 characters")
        } else {
            var unameMatch = 0
            var mailMatch = 0
            for (var i = 0; i < admins.length; i++) {
                if (username === admins[i].username) {
                    unameMatch++
                }
            }
            for (var i = 0; i < admins.length; i++) {
                if (email === admins[i].email) {
                    mailMatch++
                }
            }
            if (mailMatch === 0) {
                if (unameMatch === 0) {
                    signup(name, username, email, password, passwordConfirm, 1)
                } else {
                    showAlert("error", "Username already taken")
                }
            } else {
                showAlert("error", "Email already registered")
            }
        }
    }

})

export const signup = async (name, username, email, password, passwordConfirm, role_id) => {
    try {
        const res = await axios({
            method: "POST",
            url: "https://agro-chain.onrender.com/api/v1/users/signup",
            data: {
                name,
                username,
                email,
                password,
                passwordConfirm,
                role_id
            }
        })
        if (res.data.status === "success") {
            showAlert("success", "Account created successfully!")
            window.setTimeout(() => {
                location.assign("/admin")
            }, 1500)
        }
    } catch (err) {
        let message =
            typeof err.response !== "undefined" ? err.response.data.message : err.message
        showAlert("error", "Error: Passwords are not the same!", message)
    }
}

