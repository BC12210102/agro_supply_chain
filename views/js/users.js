import { showAlert } from "./alert.js"

const getusers = await axios({
    method: "GET",
    url: "https://agro-chain.onrender.com/api/v1/users"
})
const allusers = getusers.data.data
const users = []
allusers.forEach(user => {
    if (user.role_id === 0) {
        users.push(user)
    }
})

let useR = []
let usercount = 0
users.forEach(user => {
    // console.log(user)
    var table = document.getElementById("myTable");
    var row = document.getElementById("myTableBody").insertRow();
    var td = []
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }
    usercount += 1
    var stat;
    var act;
    var cls;
    if (user.__v === 0) {
        stat = "active"
        act = "red"
        cls = "deactivate"
    } else {
        stat = "inactive"
        act = "green"
        cls = "activate"
    }
    td[0].innerHTML = user._id;
    td[1].innerHTML = user.name;
    td[2].innerHTML = user.email;
    td[3].innerHTML = '<span class="status ' + stat + '">' + stat.charAt(0).toUpperCase() + stat.slice(1) + '</span>';
    td[4].innerHTML = '<button class="whit ' + cls + '"><ion-icon name="power"></ion-icon></button>';
    td[4].firstChild.style.backgroundColor = act
})

document.querySelectorAll(".deactivate").forEach(el => el.addEventListener("click", (e) => {
    e.preventDefault()
    let row = e.target.parentElement.parentElement.parentElement
    let id = row.firstChild.innerHTML
    deactivateUser(id)
}))
document.querySelectorAll(".activate").forEach(el => el.addEventListener("click", (e) => {
    e.preventDefault()
    let row = e.target.parentElement.parentElement.parentElement
    let id = row.firstChild.innerHTML
    activateUser(id)
}))


export const deactivateUser = async (id) => {
    try {
        const res = await axios({
            method: 'PUT',
            url: "https://agro-chain.onrender.com/api/v1/users/deactivate/" + id,
            data: {
                __v: 1
            }
        })
        if (res.data.status === 'success') {
            showAlert('success', 'User Deactivated!')
            window.setTimeout(() => {
                location.reload(true)
            }, 1000)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', err.response.data.message)
    }
}
export const activateUser = async (id) => {
    try {
        const res = await axios({
            method: 'PUT',
            url: "https://agro-chain.onrender.com/api/v1/users/deactivate/" + id,
            data: {
                __v: 0
            }
        })
        if (res.data.status === 'success') {
            showAlert('success', 'User Activated!')
            window.setTimeout(() => {
                location.reload(true)
            }, 1000)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', err.response.data.message)
    }
}
