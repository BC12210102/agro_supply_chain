import { showAlert } from "./alert.js";

const image = document.querySelector(".image");
const img = document.getElementById("pic");
const file = document.getElementById("photo");
const uploadBtn = document.getElementById("uploadBTN");

var obj = JSON.parse(document.cookie.substring(6));
var id = obj._id;
document.querySelector("#pic").src = "../img/users/" + obj.photo;
document.getElementById("name").value = obj.name;
document.getElementById("uname").value = obj.username;
document.getElementById("email").value = obj.email;

image.addEventListener("mouseenter", function () {
  uploadBtn.style.display = "block";
});
image.addEventListener("mouseleave", function () {
  uploadBtn.style.display = "none";
});

document.getElementById("photo").addEventListener("change", (e) => {
  var chosenFile = document.getElementById("photo").files[0];
  if (chosenFile) {
    const reader = new FileReader();
    reader.addEventListener("load", function () {
      document.querySelector("#pic").setAttribute("src", reader.result);
    });
    reader.readAsDataURL(chosenFile);
  }
});

document.querySelector(".form1").addEventListener("submit", (e) => {
  e.preventDefault();
  var obj = JSON.parse(document.cookie.substring(6));
  // if (document.getElementById("photo").value !== "") {
  const form = new FormData();
  const name = document.getElementById("name").value;
  const username = document.getElementById("uname").value;
  const email = document.getElementById("email").value;
  // form.append('photo', document.getElementById('photo').files[0])
  console.log(form);
  updateSettings({ name, username, email }, obj._id);
  // } else {
  //     showAlert("error", "Please select a new picture to update!")
  // }
});

const userPasswordForm = document.querySelector(".form2");
userPasswordForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  document.querySelector(".btn--save-password").textContent = "Updating...";
  var obj = JSON.parse(document.cookie.substring(6));
  const passwordCurrent = document.getElementById("currp").value;
  const password = document.getElementById("newp").value;
  const passwordConfirm = document.getElementById("cnewp").value;

  if (password.length <= 8 || passwordConfirm.length <= 8) {
    showAlert("error", "Password must be longer than 8 characters");
    document.querySelector(".btn--save-password").textContent =
      "Update password";
  } else {
    if (password !== passwordConfirm) {
      showAlert("error", "Passwords do not match");
      document.querySelector(".btn--save-password").textContent =
        "Update password";
    } else {
      updatePassword({ passwordCurrent, password, passwordConfirm }, obj._id);
      document.querySelector(".btn--save-password").textContent =
        "Update password";
      document.getElementById("currp").value = "";
      document.getElementById("newp").value = "";
      document.getElementById("cnewp").value = "";
    }
  }
});

export const updateSettings = async (data, id) => {
  try {
    const res = await axios({
      method: "PUT",
      url: "https://agro-chain.onrender.com/api/v1/users/" + id,
      data,
    });
    if (res.data.status === "success") {
      showAlert("success", "Data updated successfully!");
      window.setTimeout(() => {
        location.reload(true);
      }, 1500);
    }
  } catch (err) {
    let message =
      typeof err.response !== "Error while Updating"
        ? err.response.data.message
        : err.message;
    showAlert("error", message);
  }
};
export const updatePassword = async (data, id) => {
  try {
    const res = await axios({
      method: "PATCH",
      url: "https://agro-chain.onrender.com/api/v1/users/updateMyPassword/" + id,
      data,
    });
    if (res.data.status === "success") {
      showAlert("success", "Data updated successfully!");
      window.setTimeout(() => {
        location.reload(true);
      }, 1500);
    }
  } catch (err) {
    showAlert("error", "Incorrect current password");
  }
};
