import { showAlert } from "./alert.js"

const getusers = await axios({
    method: "GET",
    url: "https://agro-chain.onrender.com/api/v1/users"
})
const allusers = getusers.data.data

const users = []
const mail = []
allusers.forEach(user => {
    if (user.role_id === 0) {
        users.push(user)
        mail.push(user.email)
    }
})

document.querySelector(".form").addEventListener("submit", (e) => {
    e.preventDefault()
    const email = document.getElementById("email").value
    const password = document.getElementById("password").value
    let pattern = /^[^ ]+\.[gcit]{4}@[rub]{3}\.[edu]{3}\.[bt]{2}$/;
    const email_valid = (email.match(pattern))
    var c = 0
    var s;
    for (var i = 0; i < mail.length; i++) {
        if (email.toLowerCase() === mail[i]) {
            c += 1
            s = i
        }
    }
    if (email === "") {
        showAlert("error", "Please provide an email")
    } else {
        if (!email_valid) {
            showAlert("error", "Please enter a valid email")
        } else {
            if (password === "") {
                showAlert("error", "Please provide a password")
            } else {
                if (password.length <= 7) {
                    showAlert("error", "Password must be longer than 7 characters")
                } else {
                    if (c === 0) {
                        showAlert("error", "Email not registered")
                    } else {
                        if (users[s]["__v"] === 1) {
                            showAlert("error", "Cannot login. Your account is deactivated")
                        } else {
                            login(email, password)
                        }
                    }
                }
            }
        }
    }
})

const login = async (email, password) => {
    try {
        const res = await axios({
            method: "POST",
            url: "https://agro-chain.onrender.com/api/v1/users/login",
            data: {
                email,
                password,
            },
        })
        if (res.data.status === "success") {
            showAlert("success", "Logged in successfully");
            document.querySelector(".loader").classList.remove("loader--hidden");
            console.log(res.data.data.user.role_user)
            if (res.data.data.user.role_user === "BAFRA") {
              window.setTimeout(() => {
                location.assign("/home");
              }, 1500);
              var obj = res.data.data.user
              console.log(obj)
              document.cookie = "token = " + JSON.stringify(obj)
            } else {
              window.setTimeout(() => {
                location.assign("/userHome");
              }, 1500);
              var obj = res.data.data.user
              console.log(obj)
              document.cookie = "token = " + JSON.stringify(obj)
            }
        }

    } catch (err) {
        let message =
            typeof err.response !== "undefined"
                ? err.response.data.message
                : err.message
        showAlert("error", "Error: Incorrect email or password", message)
    }
}

