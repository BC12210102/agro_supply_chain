import { showAlert } from "./alert.js"

let toggle = document.querySelector(".toggle")
let navigation = document.querySelector(".navigation")
let main = document.querySelector(".main")
toggle.onclick = function () {
    navigation.classList.toggle("active")
    main.classList.toggle("active")
}

const logout = async () => {
    try {
        const res = await axios({
            method: "GET",
            url: "https://agro-chain.onrender.com/api/v1/users/logout",
        })
        if (res.data.status === "success") {
            showAlert("success", "Logged out successfully")
            window.setTimeout(() => {
                location.assign("/admin/")
            }, 1500)
        }
    } catch (err) {
        showAlert("error", "Error logging out! Try again.")
    }
}

document.getElementById("logout").addEventListener("click", (e) => logout())

var obj
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))

} else {
    document.body.innerHTML =
        "<div class='errPage'><div class='contErr'><h2>You are not logged in!!</h2></div><div><a href='/admin'>Log in</a></div></div>"
}


var el = document.querySelector(".container1")
if (obj._id) {
    el.innerHTML +=
        '<div class="card"><div class="img"><img src="../img/users/' + obj.photo + '" alt="photos" class="card-img"></div><h2>' +
        obj.name + '<br/><span>' + obj.username + '</span></h2></div></div>'

} else {
    document.body.innerHTML =
        "<div class='errPage'><div class='contErr'><h2>You are not logged in!!</h2></div><div><a href='/admin'>Log in</a></div></div>"
}