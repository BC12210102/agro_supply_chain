import { showAlert } from "./alert.js"

const logout = async () => {
    try {
        const res = await axios({
            method: "GET",
            url: "https://agro-chain.onrender.com/api/v1/users/logout",
        })
        if (res.data.status === "success") {
            showAlert("success", "Logged out successfully")
            window.setTimeout(() => {
                location.assign("/")
            }, 1500)
        }
    } catch (err) {
        showAlert("error", "Error logging out! Try again.")
    }
}

var obj
console.log(document.cookie)
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
    console.log(obj)

} else {
    showAlert("login Failed")
    document.body.innerHTML =
        "<div style='width: 225px; padding: 120px 0; margin: 0 auto;height: 100%;display: flex;flex-wrap: wrap;'><div><h2 style='color:red; font-size: 40px; text-align: center'>You are not logged in!!</h2></div><div style='display: flex; width: 100%; justify-content: space-evenly;'><a style='padding: 10px;background: #2135da;color:white;border-radius: 30px;border: 1px solid black;' href='/'>Log in</a><a style='padding: 10px;background: #2135da;color:white;border-radius: 30px;border: 1px solid black;' href='/signup'>Sign up</a></div></div>"
}

var el = document.querySelector(".container")
if (obj && obj._id) {
    el.innerHTML +=
        '<div class="card"><div class="img"><img src="../img/users/' + obj.photo + '" alt="photos" class="card-img"></div><h2>' +
        obj.name + '<br/><span>' + obj.username + '</span></h2></div><ul><a href="profileSettings.html"><li>My Profile</li></a><a id="logout"><li>Log out</li></a></ul></div>'

    var doc = document.querySelector("#logout")
    doc.addEventListener("click", (e) => logout())
} else {
    document.body.innerHTML =
        "<div style='width: 225px;padding: 120px 0; margin: 0 auto;height: 100%;display: flex;flex-wrap: wrap;'><div><h2 style='color:red; font-size: 40px; text-align: center'>You are not logged in!!</h2></div><div style='display: flex; width: 100%; justify-content: space-evenly;'><a style='padding: 10px;background: #2135da;color:white;border-radius: 30px;border: 1px solid black;' href='/'>Log in</a><a style='padding: 10px;background: #2135da;color:white;border-radius: 30px;border: 1px solid black;' href='/signup'>Sign up</a></div></div>"
}