const getusers = await axios({
    method: "GET",
    url: "https://agro-chain.onrender.com/users"
})
const allusers = getusers.data.data
const allnews = getnews.data.data
const allannouncements = getannouncements.data.data
const allcomments = getcomments.data.data
const allcommentsAnn = getcommentsAnn.data.data

const users = []
const admins = []
const news = []
let userCount = 0

allusers.forEach(user => {
    if (user.role_id === 0) {
        users.push(user)
    } else if (user.role_id === 1) {
        admins.push(user)
    }
})
users.forEach(user => {
    userCount += 1
})

document.getElementById("users").innerHTML = userCount

let adminCount = 0
admins.forEach(admin => {
    // console.log(admin)
    var table = document.getElementById("myTable");
    var row = document.getElementById("myTableBody").insertRow();
    var td = []
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }
    adminCount += 1
    var stat;
    if (admin.__v === 0) {
        stat = "active"
    } else {
        stat = "inactive"
    }
    td[0].innerHTML = adminCount;
    td[1].innerHTML = admin.name;
    td[2].innerHTML = admin.createdAt.slice(0, 10);
    td[3].innerHTML = '<span class="status ' + stat + '">' + stat.charAt(0).toUpperCase() + stat.slice(1) + '</span>';


})
