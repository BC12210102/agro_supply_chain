// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract SupplyChain {
    struct Product {
        string name;
        string quantity;
        string manufacturer;
        string productID;
        string description;
        string productionDate;
    }

    mapping(string => Product) public products;

    event ProductAdded(
        string name,
        string quantity,
        string manufacturer,
        string productID,
        string description,
        string productionDate
    );

    function addProduct(
        string memory _productID,
        string memory _name,
        string memory _quantity,
        string memory _manufacturer,
        string memory _description,
        string memory _productionDate
    ) public {
        require(
            bytes(products[_productID].productID).length == 0,
            "Product already exists"
        );

        Product memory newProduct = Product({
            name: _name,
            quantity: _quantity,
            manufacturer: _manufacturer,
            productID: _productID,
            description: _description,
            productionDate: _productionDate
        });

        products[_productID] = newProduct;

        emit ProductAdded(
            _name,
            _quantity,
            _manufacturer,
            _productID,
            _description,
            _productionDate
        );
    }

    function getProduct(
        string memory _productID
    )
        public
        view
        returns (
            string memory,
            string memory,
            string memory,
            string memory,
            string memory,
            string memory
        )
    {
        require(
            bytes(products[_productID].productID).length != 0,
            "Product does not exist"
        );

        Product memory product = products[_productID];
        return (
            product.name,
            product.quantity,
            product.manufacturer,
            product.productID,
            product.description,
            product.productionDate
        );
    }
}
