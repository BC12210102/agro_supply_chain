const path = require("path")

// USERS
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "login.html"))
}
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "signup.html"))
}
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "home.html"))
}
exports.getUserHome = (req,res)=>{
    res.sendFile(path.join(__dirname, "../","views","userHome.html"))
}


// ADMIN
exports.getAdminPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/", "admin", "dashboard.html"))
}
exports.getAdminSignup = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/", "admin", "ADMSIGNUP.html"))
}
exports.getAdminLogin = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/", "admin", "admLogin.html"))
}
exports.getUsers = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/", "admin", "userAdm.html"))
}
exports.getAdmSetting = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/", "admin", "settingAdm.html"))
}