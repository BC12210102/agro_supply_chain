const Role = require("../Models/RoleModel");

exports.getAllRoles = async (req, res) => {
    try {
        const roles = await Role.find();
        res.json({ data: roles, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.createRole = async (req, res) => {
    try {
        const role = await Role.create(req.body);
        res.json({ data: role, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.getRoleById = async (req, res) => {
    try {
        const role = await Role.findById(req.params.id);
        res.json({ data: role, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.updateRole = async (req, res) => {
    try {
        const role = await Role.findByIdAndUpdate(req.params.id, req.body);
        res.json({ data: role, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.deleteRole = async (req, res) => {
    try {
        const role = await Role.findByIdAndDelete(req.params.id);
        res.json({ data: role, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}