const User = require("../Models/UserModel");
const AppError = require("../utils/appError")
const multer = require("multer")

const storageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "views/img/users")
    },
    filename: (req, file, cb) => {
        const ext = file.mimetype.split("/")[1]
        cb(null, `users-${Date.now()}.${ext}`)
    }
})

const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image")) {
        cb(null, true)
    } else {
        cb(new AppError("Not an image! Please upload only images", 400), false)
    }
}

const upload = multer({
    storage: storageEngine,
    fileFilter: multerFilter
})

exports.uploadUserPhoto = upload.single("photo")

const filterObj = (obj, ...allowedFields) => {
    const newObj = {}
    Object.keys(obj).forEach((el) => {
        if (allowedFields.includes(el)) newObj[el] = obj[el]
    })
    return newObj
}

exports.getAllUsers = async (req, res) => {
    try {
        const users = await User.find();
        res.json({ data: users, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.createUser = async (req, res) => {
    try {
        const user = await User.create(req.body);
        res.json({ data: user, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.getUserById = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json({ data: user, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.deactivate = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body);
        res.json({ data: user, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.updateUser = async (req, res) => {
    try {
        // const filteredBody = filterObj(req.body, "name", "username", "email")
        // if (req.body.photo !== "undefined") {
        //     filteredBody.photo = req.file.filename
        // }
        const updatedUser = await User.findByIdAndUpdate(req.params.id,req.body)
        res.json({
            status: "success",
            data: updatedUser 
        })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({ data: user, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}